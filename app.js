const express = require('express');
const mongoose = require('mongoose');
const app = express();
require('dotenv/config');
app.use(express.json());

// IMPORT ROUTES
const profRoute = require('./routes/professors');

app.use('/api/professors', profRoute);


//ROUTES
app.get('/api', (req, res) => {
  res.send({
    message: "You are requesting nothing. Documentation not ready yet"
  });
})


//Connect to DB

options = {
  useUnifiedTopology: true,
  useNewUrlParser: true
}
mongoose.connect(
  process.env.DB_CONNECTION,
  options,
  () =>  console.log('Connected to DB!\n')
);


app.listen(3000, () => {
  console.log('Server Up and Running\n');
})
