const express = require('express');
const router = express.Router();
const Professor = require('../model/Professor');

//Get all Professor
router.get('/', async (req, res) => {
  try {
    const profs = await Professor.find();
    res.send(profs);
    console.log("Somebody ask for professors\n\n");
    
  } catch (e) {
    res.send({ ERR: e })
  }
});

//Create Professor
router.post('/create', async (req, res) => {
  const prof = new Professor({
    name: req.body.name,
    surname: req.body.surname,
    rating: 1000
  });

  try {
    const savedProf = await prof.save();
    res.send(savedProf);
    console.log({ New_Professor: savedProf });
    
  } catch (e) {
    res.send({ ERR: e });
    console.log({ ERR: e });
  }
});


//Delete a professor
router.delete('/del', async (req, res) => {
  try {
    const removedProf = await Professor.remove({
      name: req.body.name,
      surname: req.body.surname
    });

    res.send(removedProf);
    console.log({ professor_removed: removedProf });
    
  } catch (e) {
    res.send({ ERR: e });
  }
  

})


module.exports = router;
