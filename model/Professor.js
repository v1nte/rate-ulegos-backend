const mongoose = require('mongoose');


const profSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 6,
    max: 20,
  },

  surname: {
    type: String,
    required: true,
    min: 6,
    max: 20,
  },

  email: {
    type: String,
    required: false
  },

  rating: {
    type: Number,
    required: false
  }
});


module.exports = mongoose.model('Professor', profSchema);
