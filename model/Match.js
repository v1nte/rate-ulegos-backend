const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema({
  professor_1: {
    name: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      required: true
    }
  },

  professor_2: {
    name: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      required: true
    }
  },

  winner: {
    type: String,
    required: true
  },

  date: {
    type: Date,
    default: Date.now
  }
}); 

module.exports = mongoose.mode('Match', matchSchema);
